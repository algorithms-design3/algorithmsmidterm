/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sania.exam2;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class A1_8Reverse {
    public static void main(String[] args){
        Scanner kb = new Scanner(System.in);
        int[] arr = new int[5];
        int[] ans = new int[5];
        System.out.println("Enter the elements of the array: ");
        for(int i=0;i<5;i++){
            arr[i] = kb.nextInt();
        }
        System.out.println("Array : "+Arrays.toString(arr));
        
        long start, stop;
        start = System.nanoTime();
        
        for(int i=4;i>-1;i--){ //n=4
            ans[5-i-1] = arr[i]; //4-3-1 = 0
                                 //4-2-1 = 1
                                 //4-1-1 = 2
                                 //4-0-1 = 3
        }
        System.out.println("Reversed Array : "+Arrays.toString(ans));
        
        stop = System.nanoTime();
        System.out.println("time = " +  (stop - start) * 1E-9 +" secs.");
    }
}
