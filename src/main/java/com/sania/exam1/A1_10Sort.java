/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sania.exam1;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class A1_10Sort {
    public static void main(String[] args){
        Scanner kb = new Scanner(System.in);
        System.out.println("Enter the length of the array: ");
        int n=kb.nextInt();
        int[] arr = new int[n];
        System.out.println("Enter the elements of the array: ");
        for(int i=0;i<n;i++){
            arr[i] = kb.nextInt();
        }
        System.out.println("Array : "+Arrays.toString(arr));
        
        System.out.println("Length = "+lengthOfLongSort(arr,n));
        printArrSort(arr,n);
    }

    private static int lengthOfLongSort(int[] arr, int n) {
        int max=1; //max
        int length=1; //current
        for(int i=1 ; i<n ; i++){
            if(arr[i-1]<arr[i]){ //count
                length++;
            }else{ //arr[i-1]>arr[i]
                if(length>max){ //current>max
                    max=length;
                }
                length=1; //restart length
            }
        }
            if(length>max){ //current>max
                max=length;
            }
        
        return max;
    }

    private static void printArrSort(int[] arr, int n) {
        int max=1; //max
        int length=1; //current
        int index=0;
        
        for(int i=1 ; i<n ; i++){
            if(arr[i-1]<arr[i]){ //count
                length++;
            }else{ //arr[i-1]>arr[i]
                if(length>max){ //current>max
                    max=length;
                    index = i - max; 
                }
                length=1; //restart length
            }
        }
            if(length>max){ //current>max
                max=length;
                index = n - max;
            }
        
        for(int i=index ; i<max+index ;i++){
                System.out.print(+arr[i]+" ");
        }
    }
}
